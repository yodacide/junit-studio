package org.launchcode;

/*
* Fraction class written by Brad Miller (interactive.org)
* */
public class Fraction implements Comparable<Fraction> {

    private Integer numerator;
    private Integer denominator;

    /** Creates a new instance of Fraction */
    public Fraction(Integer num, Integer den) {
        this.numerator = num;
        this.denominator = den;
    }

    public Fraction(Integer num) {
        this.numerator = num;
        this.denominator = 1;
    }

    public Integer getNumerator() {
        return numerator;
    }

    public void setNumerator(Integer numerator) {
        this.numerator = numerator;
    }

    public Integer getDenominator() {
        return denominator;
    }

    public void setDenominator(Integer denominator) {
        this.denominator = denominator;
    }

    //this is busted
    public Fraction add(Fraction other) {
        //
        Integer newNum = other.getDenominator() * this.numerator + this.denominator * other.getNumerator();
        //
        Integer newDen = this.denominator * other.getDenominator();
        //
        int simplifiedNum = newNum / gcd(newNum, newDen);
        int simplifiedDen = newDen / gcd(newNum, newDen);



        return new Fraction(simplifiedNum, simplifiedDen);
    }

    public Fraction add(Integer other) {
        return add(new Fraction(other));
    }



    private static Integer gcd(Integer m, Integer n) {
        while (m % n != 0) {
            Integer oldm = m;
            Integer oldn = n;
            m = oldn;
            n = oldm % oldn;
        }
        return n;
    }


    public String toString() {
        return numerator.toString() + "/" + denominator.toString();
    }

    public boolean equals(Fraction other) {
        Integer num1 = this.numerator * other.getDenominator();
        Integer num2 = this.denominator * other.getNumerator();
        if (num1 == num2)
            return true;
        else
            return false;
    }

    public int compareTo(Fraction other) {
        Integer num1 = this.numerator * other.getDenominator();
        Integer num2 = this.denominator * other.getNumerator();
        return num2 - num1;
    }

}
        