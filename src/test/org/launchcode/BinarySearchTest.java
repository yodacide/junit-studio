package org.launchcode;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BinarySearchTest {

    //TODO: add tests here
    @Test
    public void binarySearchTest2(){
        int myInput[] = {1,2,23,34};
        assertEquals(1,BinarySearch.binarySearch(myInput, 2));
    }

    @Test
    public void binarySearchTest34(){
        int myInput[] = {1,2,23,34,54,87};
        assertEquals(3,BinarySearch.binarySearch(myInput, 34));
    }

    @Test
    public void binarySearchTest54(){
        int myInput[] = {1,2,23,34,54,87,100};
        assertEquals(4,BinarySearch.binarySearch(myInput, 54));
    }

    @Test
    public void binarySearchTest100(){
        int myInput[] = {1,2,23,34,54,87,100};
        assertEquals(6,BinarySearch.binarySearch(myInput, 100));
    }

    @Test
    public void binarySearchTest66(){
        int myInput[] = {1,2,23,34,54,66};
        assertEquals(5,BinarySearch.binarySearch(myInput, 66));
    }

    @Test
    public void binarySearchTestInvalidInput(){
        int myInput[] = {1,2,23,34,54,66};
        assertEquals(-1, BinarySearch.binarySearch(myInput,87));
    }
}
