package org.launchcode;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RomanNumeralTest {

    //TODO: add tests here
    @Test
    public void romanNumeralTestFor14(){
        assertEquals("XIV",RomanNumeral.fromInt(14));
    }

    @Test
    public void romanNumeralTestFor499(){
        assertEquals("CDXCIX",RomanNumeral.fromInt(499));
    }

    @Test
    public void romanNumeralTestFor444(){
        assertEquals("CDXLIV",RomanNumeral.fromInt(444));
    }

    @Test
    public void romanNumeralTestFor944(){
        assertEquals("CMXLIV",RomanNumeral.fromInt(944));
    }

    @Test
    public void romanNumeralTestFor3999(){
        assertEquals("MMMCMXCIX",RomanNumeral.fromInt(3999));
    }

}
