package org.launchcode;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/*
    learn more about math :
*/

public class FractionTest {

    //TODO: add tests here

    @Test
    public void fractionTestGetNum(){
        Fraction fraction = new Fraction(4,4);
        assertEquals(4,fraction.getNumerator().floatValue(),.001);
    }

    @Test
    public void fractionTestGetDen(){
        Fraction fraction = new Fraction(5,5);
        assertEquals(5,fraction.getDenominator().floatValue(),.001);
    }

    @Test
    public void fractionAdditionTest() {
        Fraction fraction1 = new Fraction(3, 5);
        Fraction fraction2 = new Fraction(1, 5);
        Fraction fraction3 = fraction1.add(fraction2);
        assertEquals("4/5",fraction3.toString());
    }




}



