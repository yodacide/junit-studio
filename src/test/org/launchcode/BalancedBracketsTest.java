package org.launchcode;

import org.junit.Test;

import static org.junit.Assert.*;

public class BalancedBracketsTest {

    //TODO: add tests here
    @Test
    public void balancedBracketTestJustEvenBrackets(){
        assertTrue(BalancedBrackets.hasBalancedBrackets("[]"));
    }

    @Test
    public void balanceBracketsTestJustOddBrackets(){
        assertFalse(BalancedBrackets.hasBalancedBrackets("[]["));
    }

    @Test
    public void balanceBracketTestReverseJustOddBrackets(){
        assertFalse(BalancedBrackets.hasBalancedBrackets("][]"));
    }

    @Test
    public void balancedBracketTestLaunchCodeWithBalancedBrackets(){
        assertTrue(BalancedBrackets.hasBalancedBrackets("[LaunchCode]"));
    }

    @Test
    public void balancedBracketTestLaunchCodeBackwardsBrackets(){
        assertTrue(BalancedBrackets.hasBalancedBrackets("Launch]Code["));
    }

    @Test
    public void balancedBracketTestLaunchCodeOneBracket(){
        assertFalse(BalancedBrackets.hasBalancedBrackets("[LaunchCode"));
    }

    @Test
    public void balancedBracketTestJust1Bracket(){
        assertFalse(BalancedBrackets.hasBalancedBrackets("["));
    }

    @Test
    public void balanceBracketTestJustBracketsBackwards (){
        assertTrue(BalancedBrackets.hasBalancedBrackets("]["));
    }

}
